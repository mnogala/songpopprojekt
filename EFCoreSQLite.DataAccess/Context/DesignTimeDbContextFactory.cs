﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<UsersDbEntities>
    {
        public UsersDbEntities CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<UsersDbEntities>();

            var connectionstring = @"Data Source=SongpopUsers.db;";
            builder.UseSqlite(connectionstring);

            return new UsersDbEntities(builder.Options);
        }
    }
}
