﻿using EFCoreSQLite.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess.Context
{
    public class UsersDbEntities : DbContext
    {
        public UsersDbEntities(DbContextOptions options) : base(options)
        {
        }

        public UsersDbEntities()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlaylistsEntity>()
                .ToTable("Playlists");
            modelBuilder.Entity<PlaylistsEntity>()
                .HasKey(pk => pk.Id);

            modelBuilder.Entity<UsersEntity>()
                .ToTable("User");
            modelBuilder.Entity<UsersEntity>()
                .HasKey(pk => pk.Id);

            modelBuilder.Entity<SongsEntity>()
                .ToTable("Songs");
            modelBuilder.Entity<SongsEntity>()
                .HasKey(pk => pk.Id);
            modelBuilder.Entity<SongsEntity>()
                .HasOne(o => o.Playlist)
                .WithMany(m => m.Songs)
                .HasForeignKey(fk => fk.PlaylistId);

            
        }

        public DbSet<UsersEntity> Users { get; set; }
        public DbSet<PlaylistsEntity> Playlists { get; set; }
        public DbSet<SongsEntity> Songs { get; set; }

    }
}
