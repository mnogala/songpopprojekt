﻿using EFCoreSQLite.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess
{
    public static class DatabaseConnection
    {
        public static UsersDbEntities Db()
        {
            var services = new ServiceCollection();
            services.AddDbContext<UsersDbEntities>(factory =>
                {
                    var conntectionstring = @"Data Source=SongpopUsers.db;";
                    factory.UseSqlite(conntectionstring);
                });

            var provider = services.BuildServiceProvider();

            var db = provider.GetService<UsersDbEntities>();
            return db;

        }

        
    }
}
