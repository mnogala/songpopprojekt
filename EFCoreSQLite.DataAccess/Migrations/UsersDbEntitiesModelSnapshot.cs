﻿// <auto-generated />
using EFCoreSQLite.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EFCoreSQLite.DataAccess.Migrations
{
    [DbContext(typeof(UsersDbEntities))]
    partial class UsersDbEntitiesModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.1");

            modelBuilder.Entity("EFCoreSQLite.DataAccess.Models.PlayEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("FirstSong")
                        .HasColumnType("TEXT");

                    b.Property<string>("NamePlaylist")
                        .HasColumnType("TEXT");

                    b.Property<string>("RightSong")
                        .HasColumnType("TEXT");

                    b.Property<string>("SecondSong")
                        .HasColumnType("TEXT");

                    b.Property<string>("ThirdSong")
                        .HasColumnType("TEXT");

                    b.Property<int>("WhichSong")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("Play");
                });

            modelBuilder.Entity("EFCoreSQLite.DataAccess.Models.PlaylistsEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("PlaylistName")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Playlists");
                });

            modelBuilder.Entity("EFCoreSQLite.DataAccess.Models.SongsEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Band")
                        .HasColumnType("TEXT");

                    b.Property<string>("Path")
                        .HasColumnType("TEXT");

                    b.Property<int>("PlaylistId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("PlaylistName")
                        .HasColumnType("TEXT");

                    b.Property<string>("Title")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("PlaylistId");

                    b.ToTable("Songs");
                });

            modelBuilder.Entity("EFCoreSQLite.DataAccess.Models.UsersEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Password")
                        .HasColumnType("TEXT");

                    b.Property<int>("Points")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Username")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("User");
                });

            modelBuilder.Entity("EFCoreSQLite.DataAccess.Models.SongsEntity", b =>
                {
                    b.HasOne("EFCoreSQLite.DataAccess.Models.PlaylistsEntity", "Playlist")
                        .WithMany("Songs")
                        .HasForeignKey("PlaylistId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
