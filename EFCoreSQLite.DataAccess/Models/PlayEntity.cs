﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess.Models
{
    public class PlayEntity
    {
        public int Id { get; set; }
        public string NamePlaylist { get; set; }
        public string FirstSong { get; set; }
        public string SecondSong { get; set; }
        public string ThirdSong { get; set; }
        public string RightSong { get; set; }
        public int WhichSong { get; set; }

    }
}
