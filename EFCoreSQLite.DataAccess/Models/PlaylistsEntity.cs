﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess.Models
{
    public class PlaylistsEntity
    {
        public int Id { get; protected set; }
        public string PlaylistName { get; set; }

        public List<SongsEntity> Songs { get; set; }
    }
}
