﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess.Models
{
    public class SongsEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Band { get; set; }
        public string Path { get; set; }
        public string PlaylistName { get; set; }

        public int PlaylistId  { get; set; }
        public PlaylistsEntity Playlist { get; set; }
    }
}
