﻿using EFCoreSQLite.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreSQLite.DataAccess.Models
{
    public class UsersEntity
    {
        public int Id { get; protected set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Points { get; set; }

        public void AddUser(string user,string pass)
        {
            UsersDbEntities db = DatabaseConnection.Db();

            var userNew = new UsersEntity()
            {
                Username = user,
                Password = pass
            };

            db.Users.Add(userNew);
            db.SaveChanges();
        }

    }
}
