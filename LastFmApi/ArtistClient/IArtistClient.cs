﻿using LastFmApi.ArtistInfo;
using RestEase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LastFmApi.ArtistClient
{
    public interface IArtistClient
    {
        [Get]
        Task<NewArtist> GetArtist([Query("method")] string method, string artist, string api_key, string format);
    }
}
