﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastFmApi.ArtistInfo
{
    public class Artist
    {
        public string name { get; set; }
        public Bio bio { get; set; }
    }
}
