﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastFmApi.ArtistInfo
{
    public class Bio
    {
        public string summary { get; set; }
        public string content { get; set; }
    }
}