﻿using LastFmApi.ArtistClient;
using LastFmApi.ArtistInfo;
using RestEase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LastFmApi
{
    public abstract class LastFmClient
    {
        private static IArtistClient artistClient;

        private static async Task<NewArtist> GetArtistInfo()
        {
            string artist = "Megadeth";
            string method = "artist.getinfo";
            string api_key = "4a52414ed854a89680b53f6594d49f39";
            string url = "http://ws.audioscrobbler.com/2.0/";
            artistClient = RestClient.For<IArtistClient>(url);

            NewArtist responseArtist = await artistClient.GetArtist(method, artist, api_key, "json");

            return responseArtist;

        }

        public static string GetBio()
        {
            Task<NewArtist> getBio = GetArtistInfo();
            return getBio.Result.artist.bio.summary.ToString();
        }
      

    }
}