﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using EFCoreSQLite.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WinformsCore
{
    public partial class Form1 : Form
    {
        private Mp3Player mp3Player = new Mp3Player();
        List<SongsEntity> newPlaylistToDatabase= new List<SongsEntity>();
        static int counter=0;
        UsersDbEntities db = DatabaseConnection.Db();
        bool access;

        public Form1()
        {
            InitializeComponent();
        }



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            tbPassword.PasswordChar = '*';
        }

        private void tbUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string login = tbUsername.Text;
            string password = tbPassword.Text;
            var childForm = new Game();
            int tmp=0;
            string select;
            if (lvPlaylists.SelectedItems.Count==0)
            {
                MessageBox.Show("You must chose playlist to log in");
            }
            else
            {
                select= lvPlaylists.SelectedItems[0].Text;
                Points.playlistName = select;
                if (String.IsNullOrWhiteSpace(tbUsername.Text) || String.IsNullOrWhiteSpace(tbPassword.Text))
                {
                    MessageBox.Show("Enter username and password");
                }
                else
                {
                    foreach (var us in db.Users)
                    {
                        if (us.Username == login)
                        {
                            if (us.Password == password)
                            {

                                Points.AddSongsToGame(select);
                                MessageBox.Show($"Logged In. Selected playlist: {select}");
                                access = true;
                                tmp = 1;
                                break;
                            }
                            else if (us.Password != password)
                            {
                                MessageBox.Show("Wrong password");
                                access = false;
                                tmp = 1;
                            }
                        }

                    }
                    if (tmp == 0)
                    {
                        UsersEntity CreateUser = new UsersEntity();
                        CreateUser.AddUser(login, password);
                        access = true;
                        Points.AddSongsToGame(select);
                        MessageBox.Show($"New user created. Selected playlist: {select}");

                    }

                    newPlaylistToDatabase.Clear();
                    if (access == true)
                    {

                        this.Hide();
                        childForm.ShowDialog();
                        childForm.BringToFront();
                        this.Close();

                    }
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lvPlaylists.View = View.Details;
            lvPlaylists.GridLines = true;
            lvPlaylists.FullRowSelect = true;

            lvPlaylists.Columns.Add("Playlists", 230);
          
            foreach (var p in db.Playlists)
            {
                var item = new ListViewItem();
                item.Text = p.PlaylistName;
                lvPlaylists.Items.Add(item);
            }
            if (lvPlaylists.Items.Count == 0)
            {
              
            }
            else
            {
                lvPlaylists.Items[0].Selected = true;
            }
            lvSongs.View = View.Details;
            lvSongs.GridLines = true;
            lvSongs.FullRowSelect = true;

            lvSongs.Columns.Add("Songs", 115);
            lvSongs.Columns.Add("Band", 115);

            tbTitle.Clear();
            tbBand.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {


        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbPlaylistName.Text))
            {
                MessageBox.Show("Enter playlist name");
            }
            else
            {
                if (counter < 9)
                {

                    MessageBox.Show("Add more songs.");
                }
                else
                {
                    foreach (var s in newPlaylistToDatabase)
                    {
                        s.PlaylistName = tbPlaylistName.Text;
                    }
                    var playlist = new PlaylistsEntity
                    {
                        PlaylistName = tbPlaylistName.Text
                    };

                    if (playlist.Songs == null)
                    {
                        playlist.Songs = new List<SongsEntity>();
                    }

                    db.Playlists.Add(playlist);
                    foreach (var s in newPlaylistToDatabase)
                    {
                        playlist.Songs.Add(s);
                    }
                    db.SaveChanges();

                    lvPlaylists.Items.Clear();

                    foreach (var p in db.Playlists)
                    {
                        var item = new ListViewItem();
                        item.Text = p.PlaylistName;
                        lvPlaylists.Items.Add(item);
                    }
                    lvPlaylists.Items[0].Selected = true; 
                    tbPlaylistName.Clear();
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbPlaylistName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnAddSong_Click(object sender, EventArgs e)
        {
            string filepath;
            if (String.IsNullOrWhiteSpace(tbBand.Text) || String.IsNullOrWhiteSpace(tbTitle.Text))
            {
                MessageBox.Show("Enter band name and title first");
                    }
            else { 

                using (OpenFileDialog ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Mp3 Files|*.mp3";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        mp3Player.open(ofd.FileName);
                    }
                    
                    filepath = FilePath.GetPath(ofd.FileName);
                    
                }

                if (String.IsNullOrEmpty(filepath))
                {
                    MessageBox.Show("File path cannot be empty.");
                }
                else
                {
                    var song = new SongsEntity
                    {
                        Title = tbTitle.Text,
                        Band = tbBand.Text,
                        Path = FilePath.fPath

                    };
                    var songs = new SongsEntity
                    {
                        Title = tbTitle.Text,
                        Band = tbBand.Text,
                        Path = FilePath.fPath


                    };

                    newPlaylistToDatabase.Add(song);


                    lvSongs.Items.Clear();

                    foreach (var p in newPlaylistToDatabase)
                    {
                        var item = new ListViewItem();
                        item.Text = p.Title;
                        item.SubItems.Add(p.Band);
                        item.SubItems.Add(p.PlaylistName);
                        lvSongs.Items.Add(item);
                    }
                    lvSongs.Items[0].Selected = true;
                    tbTitle.Clear();
                    tbBand.Clear();
                    counter++;
                }
            }
        }
   
    }
}


