﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using System;
using System.Windows.Forms;

namespace WinformsCore
{
    public partial class Game : Form
    {
        UsersDbEntities db = DatabaseConnection.Db();

        private Mp3Player mp3Player = new Mp3Player();

        public Game()
        {
            InitializeComponent();
        }

        private void Game_Load(object sender, EventArgs e)
        {
            
            btnSelect1.Text = Points.newGame[0].FirstSong.ToString(); 
            btnSelect2.Text = Points.newGame[0].SecondSong.ToString(); 
            btnSelect3.Text = Points.newGame[0].ThirdSong.ToString(); 


           
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            mp3Player.stop();
            
            var childForm = new Game1();
            Points.addPoints(Points.newGame[0].WhichSong, 1); 

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
            

        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            
            foreach (var p in db.Songs)
            { 
                if (p.Title == Points.newGame[0].RightSong&& p.PlaylistName== Points.playlistName)
                {
                    mp3Player.open(p.Path);
                    Points.bandsList.Add(p);
                   
                }
               
            }
            mp3Player.play();
        }

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            mp3Player.stop();
            /*Jeśli piosenka będzie poprawna to wykonaj*/

            Points.addPoints(Points.newGame[0].WhichSong, 2);
            var childForm = new Game1();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
        }

        private void btnSelect3_Click(object sender, EventArgs e)
        {
            mp3Player.stop();
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            Points.addPoints(Points.newGame[0].WhichSong, 3);
            
            var childForm = new Game1();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
        }
    }
}

