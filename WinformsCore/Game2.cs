﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using EFCoreSQLite.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinformsCore
{
    public partial class Game2 : Form
    {
        UsersDbEntities db = DatabaseConnection.Db();
        private Mp3Player mp3Player = new Mp3Player();
        public Game2()
        {
            InitializeComponent();
        }

        private void Game2_Load(object sender, EventArgs e)
        {
            btnSelect1.Text = Points.newGame[2].FirstSong.ToString();
            btnSelect2.Text = Points.newGame[2].SecondSong.ToString();
            btnSelect3.Text = Points.newGame[2].ThirdSong.ToString();

            foreach (var p in db.Songs)
            {
                if (p.Title == Points.newGame[2].RightSong && p.PlaylistName == Points.playlistName)
                {
                    mp3Player.open(p.Path);
                    Points.bandsList.Add(p);
                    

                }
            }
            mp3Player.play();
            lblPoints.Text = Points.points.ToString();
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            mp3Player.stop();
            Points.addPoints(Points.newGame[2].WhichSong, 1);
            var childForm = new Game3();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
            
        }

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            mp3Player.stop();
            Points.addPoints(Points.newGame[2].WhichSong, 2);
            var childForm = new Game3();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
            
        }

        private void btnSelect3_Click(object sender, EventArgs e)
        {
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            mp3Player.stop();
            Points.addPoints(Points.newGame[2].WhichSong, 3);
            var childForm = new Game3();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
        }
    }
}
