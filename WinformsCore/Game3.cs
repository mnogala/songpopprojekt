﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using System;
using System.Windows.Forms;

namespace WinformsCore
{
    public partial class Game3 : Form
    {
        UsersDbEntities db = DatabaseConnection.Db();
        private Mp3Player mp3Player = new Mp3Player();
        public Game3()
        {
            InitializeComponent();
        }

        private void Game3_Load(object sender, EventArgs e)
        {
            btnSelect1.Text = Points.newGame[3].FirstSong.ToString();
            btnSelect2.Text = Points.newGame[3].SecondSong.ToString();
            btnSelect3.Text = Points.newGame[3].ThirdSong.ToString();

            foreach (var p in db.Songs)
            {
                if ((p.Title == Points.newGame[3].RightSong && p.PlaylistName == Points.playlistName))
                {
                    mp3Player.open(p.Path);
                    Points.bandsList.Add(p);
                   
                }
            }
            mp3Player.play();
            lblPoints.Text = Points.points.ToString();
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            mp3Player.stop();
            Points.addPoints(Points.newGame[3].WhichSong, 1);
            var childForm = new Game4();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
            
        }

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            mp3Player.stop();
            Points.addPoints(Points.newGame[3].WhichSong, 2);
            var childForm = new Game4();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
            
        }

        private void btnSelect3_Click(object sender, EventArgs e)
        {
            /*Jeśli piosenka będzie poprawna to wykonaj*/
            mp3Player.stop();
            Points.addPoints(Points.newGame[3].WhichSong, 3);
            var childForm = new Game4();

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
            
        }
    }
}
