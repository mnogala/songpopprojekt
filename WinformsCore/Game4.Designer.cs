﻿namespace WinformsCore
{
    partial class Game4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelect1 = new System.Windows.Forms.Button();
            this.btnSelect2 = new System.Windows.Forms.Button();
            this.btnSelect3 = new System.Windows.Forms.Button();
            this.lblPointsName = new System.Windows.Forms.Label();
            this.lblPoints = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSelect1
            // 
            this.btnSelect1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(191)))));
            this.btnSelect1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(191)))));
            this.btnSelect1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(174)))), ((int)(((byte)(167)))));
            this.btnSelect1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect1.Location = new System.Drawing.Point(132, 139);
            this.btnSelect1.Margin = new System.Windows.Forms.Padding(8);
            this.btnSelect1.Name = "btnSelect1";
            this.btnSelect1.Size = new System.Drawing.Size(227, 36);
            this.btnSelect1.TabIndex = 0;
            this.btnSelect1.Text = "Good";
            this.btnSelect1.UseVisualStyleBackColor = false;
            this.btnSelect1.Click += new System.EventHandler(this.btnSelect1_Click);
            // 
            // btnSelect2
            // 
            this.btnSelect2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(191)))));
            this.btnSelect2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(191)))));
            this.btnSelect2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(174)))), ((int)(((byte)(167)))));
            this.btnSelect2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect2.Location = new System.Drawing.Point(132, 261);
            this.btnSelect2.Margin = new System.Windows.Forms.Padding(8);
            this.btnSelect2.Name = "btnSelect2";
            this.btnSelect2.Size = new System.Drawing.Size(227, 36);
            this.btnSelect2.TabIndex = 1;
            this.btnSelect2.Text = "Good";
            this.btnSelect2.UseVisualStyleBackColor = false;
            this.btnSelect2.Click += new System.EventHandler(this.btnSelect2_Click);
            // 
            // btnSelect3
            // 
            this.btnSelect3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(191)))));
            this.btnSelect3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(191)))));
            this.btnSelect3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(174)))), ((int)(((byte)(167)))));
            this.btnSelect3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect3.Location = new System.Drawing.Point(132, 199);
            this.btnSelect3.Margin = new System.Windows.Forms.Padding(8);
            this.btnSelect3.Name = "btnSelect3";
            this.btnSelect3.Size = new System.Drawing.Size(227, 36);
            this.btnSelect3.TabIndex = 2;
            this.btnSelect3.Text = "Good";
            this.btnSelect3.UseVisualStyleBackColor = false;
            this.btnSelect3.Click += new System.EventHandler(this.btnSelect3_Click);
            // 
            // lblPointsName
            // 
            this.lblPointsName.AutoSize = true;
            this.lblPointsName.Location = new System.Drawing.Point(139, 82);
            this.lblPointsName.Margin = new System.Windows.Forms.Padding(0);
            this.lblPointsName.Name = "lblPointsName";
            this.lblPointsName.Size = new System.Drawing.Size(117, 28);
            this.lblPointsName.TabIndex = 3;
            this.lblPointsName.Text = "Your points:";
            // 
            // lblPoints
            // 
            this.lblPoints.AutoSize = true;
            this.lblPoints.Location = new System.Drawing.Point(285, 82);
            this.lblPoints.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblPoints.Name = "lblPoints";
            this.lblPoints.Size = new System.Drawing.Size(66, 28);
            this.lblPoints.TabIndex = 4;
            this.lblPoints.Text = "Points";
            // 
            // Game4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(236)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(474, 338);
            this.Controls.Add(this.btnSelect2);
            this.Controls.Add(this.btnSelect3);
            this.Controls.Add(this.btnSelect1);
            this.Controls.Add(this.lblPoints);
            this.Controls.Add(this.lblPointsName);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Game4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game4";
            this.Load += new System.EventHandler(this.Game4_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelect1;
        private System.Windows.Forms.Button btnSelect2;
        private System.Windows.Forms.Button btnSelect3;
        private System.Windows.Forms.Label lblPointsName;
        private System.Windows.Forms.Label lblPoints;
    }
}