﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using System;
using System.Windows.Forms;

namespace WinformsCore
{
    public partial class Game4 : Form
    {
        UsersDbEntities db = DatabaseConnection.Db();
        private Mp3Player mp3Player = new Mp3Player();
        LastFm childForm = new LastFm();
        public Game4()
        {
            InitializeComponent();
        }

        private void Game4_Load(object sender, EventArgs e)
        {
            btnSelect1.Text = Points.newGame[4].FirstSong.ToString();
            btnSelect2.Text = Points.newGame[4].SecondSong.ToString();
            btnSelect3.Text = Points.newGame[4].ThirdSong.ToString();

            foreach (var p in db.Songs)
            {
                if (p.Title == Points.newGame[4].RightSong && p.PlaylistName == Points.playlistName)
                {
                    mp3Player.open(p.Path);
                    Points.bandsList.Add(p);
                  
                }
            }
            mp3Player.play();
            lblPoints.Text = Points.points.ToString();
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            mp3Player.stop();
            Points.addPoints(Points.newGame[4].WhichSong, 1);
            MessageBox.Show($"Congratulations!\nYou have scored {Points.points}!");

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();

        }

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            mp3Player.stop();
            Points.addPoints(Points.newGame[4].WhichSong, 2);
            MessageBox.Show($"Congratulations!\nYou have scored {Points.points}!");

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();

        }

        private void btnSelect3_Click(object sender, EventArgs e)
        {
            mp3Player.stop();
            Points.addPoints(Points.newGame[4].WhichSong, 3);
            MessageBox.Show($"Congratulations!\nYou have scored {Points.points}!");

            this.Hide();
            childForm.ShowDialog();
            childForm.BringToFront();
            this.Close();
        }
    }
}
