﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WinformsCore
{
    public static class GameInfo
    {
        
        public static int[] game(int min, int max, int notEqual, int howMany)
        {
            int[] randArr = new int[10];
            Random rand = new Random();
            int random;
            bool different = true;
            for (int i = 0; i < howMany; i++)
            {
                random = rand.Next(min, max);
                randArr[i] = random;
                do
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (randArr[i] == randArr[j] || randArr[i] == notEqual)
                        {
                            different = false;
                            random = rand.Next(min, max);
                            randArr[i] = random;
                            break;
                            
                        }
                        else { different = true; }
                    }
                } while (different == false);

            }
            return randArr;
        }

        public static int oneRand(int min, int max, int notEqual1, int notEqual2)
        {
            Random rand= new Random();
            int random; 
            do
            {
                 random = rand.Next(min, max);
            } while (random == notEqual1 || random ==notEqual2);
            return random; 
        }
    }
}
