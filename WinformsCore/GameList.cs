﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using EFCoreSQLite.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace WinformsCore
{
    public class GameList
    {
        public string playlistName { get; set; }
        public List<SongsEntity> listGame { get; set; }
        public void gameList(string playlistName)
        {
            UsersDbEntities db = DatabaseConnection.Db();
            List<SongsEntity> listGame = new List<SongsEntity>(); 
            foreach (var s in db.Songs)
            {

                if (s.PlaylistName == playlistName)
                {
                    this.listGame.Add(s);
                    
                }
            }
            this.playlistName = playlistName; 
        }
    }
}
