﻿namespace WinformsCore
{
    partial class LastFm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblQuestion = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.btnShowApi = new System.Windows.Forms.Button();
            this.lblBandBio = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblQuestion
            // 
            this.lblQuestion.AutoSize = true;
            this.lblQuestion.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblQuestion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.lblQuestion.Location = new System.Drawing.Point(38, 11);
            this.lblQuestion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblQuestion.Name = "lblQuestion";
            this.lblQuestion.Size = new System.Drawing.Size(681, 28);
            this.lblQuestion.TabIndex = 0;
            this.lblQuestion.Text = "Do you want to show some information about artists you heard in the game?";
            this.lblQuestion.Click += new System.EventHandler(this.lblQuestion_Click);
            // 
            // listView1
            // 
            this.listView1.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.listView1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(188, 50);
            this.listView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(456, 113);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // btnShowApi
            // 
            this.btnShowApi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(166)))), ((int)(((byte)(139)))));
            this.btnShowApi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(166)))), ((int)(((byte)(139)))));
            this.btnShowApi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(174)))), ((int)(((byte)(167)))));
            this.btnShowApi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowApi.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnShowApi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.btnShowApi.Location = new System.Drawing.Point(356, 175);
            this.btnShowApi.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnShowApi.Name = "btnShowApi";
            this.btnShowApi.Size = new System.Drawing.Size(122, 35);
            this.btnShowApi.TabIndex = 2;
            this.btnShowApi.Text = "Show me!";
            this.btnShowApi.UseVisualStyleBackColor = false;
            this.btnShowApi.Click += new System.EventHandler(this.btnShowApi_Click);
            // 
            // lblBandBio
            // 
            this.lblBandBio.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBandBio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.lblBandBio.Location = new System.Drawing.Point(38, 241);
            this.lblBandBio.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBandBio.Name = "lblBandBio";
            this.lblBandBio.Size = new System.Drawing.Size(726, 170);
            this.lblBandBio.TabIndex = 3;
            // 
            // LastFm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(236)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(799, 450);
            this.Controls.Add(this.lblBandBio);
            this.Controls.Add(this.btnShowApi);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.lblQuestion);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "LastFm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LastFm";
            this.Load += new System.EventHandler(this.LastFm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblQuestion;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnShowApi;
        private System.Windows.Forms.Label lblBandBio;
    }
}