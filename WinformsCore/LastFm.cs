﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using LastFmApi.ArtistClient;
using LastFmApi.ArtistInfo;
using RestEase;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformsCore
{
    public partial class LastFm : Form
    {
        UsersDbEntities db = DatabaseConnection.Db();
        private static IArtistClient artistClient;
        
        public LastFm()
        {
            InitializeComponent();
        }
        public static async Task<NewArtist> GetArtistInfo(string artist)
        {
            string method = "artist.getinfo";
            string api_key = "4a52414ed854a89680b53f6594d49f39";
            string url = "http://ws.audioscrobbler.com/2.0/";
            artistClient = RestClient.For<IArtistClient>(url);

            var responseArtist = await artistClient.GetArtist(method, artist, api_key, "json");
            return responseArtist;

        }

        private void LastFm_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
            
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            listView1.Columns.Add("Band", 216);
            listView1.Columns.Add("Song Title", 216);

            foreach (var b in Points.bandsList)
            {
                        var item = new ListViewItem();
                        item.Text = b.Band;
                        item.SubItems.Add(b.Title);
                        listView1.Items.Add(item);
                    
               
            }
            listView1.Items[0].Selected = true; 

        }

        private async void btnShowApi_Click(object sender, EventArgs e)
        {
            string select = listView1.SelectedItems[0].Text;
            Task<NewArtist> task = GetArtistInfo(select);
            NewArtist artist = await task;
            lblBandBio.Text = artist.artist.bio.summary.ToString();
        }

        private void lblQuestion_Click(object sender, EventArgs e)
        {

        }
    }
}
