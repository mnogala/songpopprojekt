﻿using EFCoreSQLite.DataAccess;
using EFCoreSQLite.DataAccess.Context;
using EFCoreSQLite.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace WinformsCore
{
    public static class Points
    {
        public static int points = 0;
        public static List<PlayEntity> newGame=new List<PlayEntity>();
        public static List<SongsEntity> gameSongs = new List<SongsEntity>();
        public static List<SongsEntity> bandsList = new List<SongsEntity>(); 
        public static string playlistName; 
        public static int arrIndex = 0; 
        public static int first;
        public static int second;
        public static int third;
        public static int which;
        public static string guess; 
        public static void addPoints(int which, int button)
        {
            if (which == button)
            {
                Points.points+=100; 
            }
        }

        public static void AddSongsToGame(string select)
        {

            int[] guessIndexArr = new int[5];
            int firstOther;
            int secondOther;
            UsersDbEntities db = DatabaseConnection.Db();

            foreach (var s in db.Songs)
            {

                if (s.PlaylistName == select)
                {
                    gameSongs.Add(s);
                }
            }
            guessIndexArr = GameInfo.game(0, gameSongs.Count, -1, 5);
            for (int i = 0; i < 5; i++)
            {
                firstOther = GameInfo.oneRand(0, gameSongs.Count, -1, guessIndexArr[i]);
                secondOther = GameInfo.oneRand(0, gameSongs.Count, firstOther, guessIndexArr[i]);
                which = GameInfo.oneRand(1, 3, -1, -1);
                if (which == 1)
                {
                    first = guessIndexArr[i];
                    guess = gameSongs[guessIndexArr[i]].Title;
                    second = firstOther;
                    third = secondOther;
                }
                else if (which == 2)
                {
                    first = firstOther;
                    second = guessIndexArr[i];
                    guess = gameSongs[guessIndexArr[i]].Title;
                    third = secondOther;

                }
                else if (which == 3)
                {
                    first = firstOther;
                    second = secondOther;
                    third = guessIndexArr[i];
                    guess = gameSongs[guessIndexArr[i]].Title;

                }


                var play = new PlayEntity()
                {
                    NamePlaylist = select,
                    FirstSong = gameSongs[first].Title,
                    SecondSong = gameSongs[second].Title,
                    ThirdSong = gameSongs[third].Title,
                    RightSong = guess,
                    WhichSong = which

                };
                newGame.Add(play);

            }
        }

      
    }
}
